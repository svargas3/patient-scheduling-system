package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.BiConsumer;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.table.AbstractTableModel;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;
import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.Visit;
import edu.miami.cis324.hw4.svargas.data.VisitComp;

public class EditVisit extends JPanel {
	private static final long serialVersionUID = 1L;
	private class VisitTableModel extends AbstractTableModel{
		private static final long serialVersionUID = 1L;
		private SchedulerData schedulerData;
		ArrayList<Visit<Integer,Integer>> vList;
		private String[] columnNames = {"Date", "Time", "Patient", "Doctor", "Remove?"};
		private boolean isRemoved = false;
		
		public VisitTableModel(SchedulerData sd)
		{
			schedulerData = sd;
			vList = (ArrayList<Visit<Integer, Integer>>) schedulerData.getVisitList();
			Collections.sort((List<Visit<Integer,Integer>>)vList, new VisitComp<Integer, Integer>());
		}
		
		@Override
		public boolean isCellEditable(int row, int col)
		{
			if(col == 0 || col == 4)
				return true;
			return false;
		}
		
		@Override
		public String getColumnName(int columnIndex)
		{
			return columnNames[columnIndex];
		}
		
		@Override 
		public int getColumnCount()
		{
			return 5;
		}
		
		@Override
		public int getRowCount()
		{
			return vList.size();
		}
		
		@Override
		public Object getValueAt(int rowIndex, int columnIndex)
		{
			Visit<Integer, Integer> v = vList.get(rowIndex);
			Patient p = schedulerData.findPatient(v.getVisitor());
			Doctor d = schedulerData.findDoctor(v.getHost());
			switch(columnIndex){
			case 0:
				return MyUtils.convertDateToString(v.getVisitDate(), dateFormat);
			case 1:
				return MyUtils.convertDateToString(v.getVisitDate(), timeFormat);
			case 2:
				return p.toString();
			case 3: 
				return d.toString();
			case 4:
				return isRemoved;
			}
			return null;
		}
		
		@Override
		public void setValueAt(Object value, int row, int col)
		{
			if(col == 4)
			{
				int result = Menu.showConfirmationDialog("Are you sure you wish to remove this visit?");
				if(result == JOptionPane.YES_OPTION)
				{
					vList.remove(row);
					fireTableRowsDeleted(row, row);
				}
			}
		}
		
		@Override
		   public Class<?> getColumnClass(int columnIndex){
		          switch (columnIndex){
		             case 0:
		            	 return String.class;
		             case 1:
		            	 return String.class;
		             case 2:
		               return Patient.class;
		             case 3:
		               return Doctor.class;
		             case 4:
		               return Boolean.class;
		             }
		             return null;
		      } 
	}
	public static String title = "Edit Visit";
	private GroupLayout listLayout;
	private JLabel header = new JLabel("Choose Visit to Edit");
	private JScrollPane scroller = new JScrollPane();
	private JTable visitTable;
	private String dateFormat = "MM/dd/yyyy";
	private String timeFormat = "hh:mm a";
	private Font headerFont = new Font("Sitka Heading", Font.BOLD, 18);
	
	public Date editDateDialog()
	{
		DatePicker datePicker = new DatePicker();
		JFrame frame = new JFrame();
		frame.getContentPane().add(datePicker);
		frame.add(datePicker);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 250, 150);
		JOptionPane.showConfirmDialog(frame, "Choose a new date for vist");
		return datePicker.getDate();
	}
	/**
	 * Create the panel.
	 */
	public EditVisit(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {
		listLayout = new GroupLayout(this);
		listLayout.setAutoCreateGaps(true);
		listLayout.setAutoCreateContainerGaps(true);
		setLayout(listLayout);
		
		header.setAlignmentX(0.5f);
		header.setFont(headerFont);
		
		visitTable = new JTable();
		
		visitTable.setModel(new VisitTableModel(sd));
		scroller.setViewportView(visitTable);
		
		listLayout.setHorizontalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(header)
					.addComponent(scroller)));
		
		listLayout.setVerticalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(header))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(scroller)));
	
	}
}
