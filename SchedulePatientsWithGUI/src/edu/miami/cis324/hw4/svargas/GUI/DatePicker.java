package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.stream.IntStream;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;

import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;

import javax.swing.GroupLayout.Alignment;

public class DatePicker extends JPanel {

	private JComboBox<String> month;
	private JComboBox<Integer> day;
	private JComboBox<Integer> year;
	private static String format = "MMMM dd, yyyy";
	private String list[] = {"January", "February", "March", "April", "May", "June", "July",
			"August", "September", "October", "November", "December"};
	private String ampmOptions[] = {"AM", "PM"};
	private int daysPerMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	private Integer yearOptions[] = IntStream.rangeClosed(1950,2050).boxed().toArray(Integer[]::new);
	private GroupLayout dateLayout;
	
	/**
	 * Create the panel.
	 */
	public DatePicker() {
		dateLayout = new GroupLayout(this);
		dateLayout.setAutoCreateGaps(true);
		
		setLayout(dateLayout);
		
		DefaultComboBoxModel<Integer> dayModel = new DefaultComboBoxModel<Integer>();
		for(int i = 1; i <= daysPerMonth[0]; i++)
		{
			dayModel.addElement(Integer.valueOf(i));
		}
		month = new JComboBox<String>(list);
		day = new JComboBox<Integer>();
		day.setModel(dayModel);
		
		year = new JComboBox<Integer>(yearOptions);
		
		month.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						int index = month.getSelectedIndex();
						int dayCount = daysPerMonth[index];
						int currentDayCount = dayModel.getSize();
						if(currentDayCount < dayCount)
						{
							while(currentDayCount < dayCount)
							{
								currentDayCount++;
								dayModel.addElement(Integer.valueOf(currentDayCount));
							}
						}
						else if(currentDayCount > dayCount)
						{
							while(currentDayCount > dayCount)
							{
								currentDayCount--;
								dayModel.removeElementAt(currentDayCount);
							}
						}
						
					}
				});
		
		year.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						int yearSelected = (Integer)year.getSelectedItem();
						int currentDayCount = dayModel.getSize();
						if(yearSelected % 4 == 0 && (yearSelected % 100 != 0 || yearSelected % 400 == 0))
						{	
							daysPerMonth[1] = 29;
							if(currentDayCount == 28)
								dayModel.addElement(daysPerMonth[1]);
						}
						else
						{
							daysPerMonth[1] = 28;
							if(currentDayCount == 29)
								dayModel.removeElement(29);
						}
						
					}
				});
	

		dateLayout.setHorizontalGroup(dateLayout.createSequentialGroup()
				.addGroup(dateLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(month))
				.addGroup(dateLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(day))
				.addGroup(dateLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(year)));
		
		dateLayout.setVerticalGroup(dateLayout.createSequentialGroup()
				.addGroup(dateLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(month)
						.addComponent(day)
						.addComponent(year)));
	}
	
	public Date getDate()
	{
		String selectedDate = month.getSelectedItem() + " " + (day.getSelectedIndex() + 1) + ", " + year.getSelectedItem();
		return MyUtils.convertStringToDate(selectedDate, format);
	}
}
