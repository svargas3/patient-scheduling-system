package edu.miami.cis324.hw4.svargas.GUI;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.function.BiConsumer;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.data.Doctor;

public class ListDoctors extends JPanel {

	public static String title = "Edit Doctor";
	private GroupLayout listLayout;
	Font activeFont = new Font("Arial", Font.PLAIN, 14);
	Font removedFont = new Font("Arial", Font.ITALIC, 14);
	private JLabel header = new JLabel("Choose Doctor to Edit");
	private JScrollPane removedScroller;
	private JScrollPane activeScroller;
	private JSplitPane splitPane;
	private JCheckBox showActive, showRemoved;
	private JButton submit;
	
	/**
	 * Create the panel.
	 */
	public ListDoctors(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {

		listLayout = new GroupLayout(this);
		listLayout.setAutoCreateGaps(true);
		listLayout.setAutoCreateContainerGaps(true);
		setLayout(listLayout);
		
		header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setAlignmentX(0.5f);
		header.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		
		removedScroller = new JScrollPane();
		activeScroller = new JScrollPane();
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, activeScroller, removedScroller);
		splitPane.setOneTouchExpandable(true);
		splitPane.getRightComponent().setVisible(false);
		
		JList<Doctor> activeDoctorList = new JList<Doctor>();
		JList<Doctor> removedDoctorList = new JList<Doctor>();
		//scrollpane constraints
		
		
		showActive = new JCheckBox("Active");
		showActive.setSelected(true);
		showRemoved = new JCheckBox("Removed");
		showRemoved.setAlignmentX(RIGHT_ALIGNMENT);
		
		submit = new JButton("Edit Doctor");
		submit.setBackground(Color.GREEN);
		
		Collection<Doctor> dList = sd.getDoctorList();
		DefaultListModel<Doctor> activeDLM = new DefaultListModel<Doctor>();
		DefaultListModel<Doctor> removedDLM = new DefaultListModel<Doctor>();
		for(Doctor d : dList){
			if(d.isRemoved() == true)
				removedDLM.addElement(d);
			else
				activeDLM.addElement(d);
		}
		activeDoctorList.setFont(activeFont);
		activeDoctorList.setModel(activeDLM);
		removedDoctorList.setFont(removedFont);
		removedDoctorList.setModel(removedDLM);
		
		activeScroller.setViewportView(activeDoctorList);
		removedScroller.setViewportView(removedDoctorList);
		
		listLayout.setHorizontalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(header)
					.addGroup(listLayout.createSequentialGroup()
							.addComponent(showActive)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
				                     GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(showRemoved))
					.addComponent(splitPane)
					.addComponent(submit)));
		
		listLayout.setVerticalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(header))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(showActive)
						.addComponent(showRemoved))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(splitPane))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(submit)));
		showRemoved.addActionListener(new ActionListener()
				{

					@Override
					public void actionPerformed(ActionEvent arg0) {
						if(splitPane.getRightComponent().isVisible() == false)
						{
							splitPane.getRightComponent().setVisible(true);
							splitPane.setDividerLocation(MainWindow.windowWidth/2 - 20);
						}
						else
						{	
							splitPane.getRightComponent().setVisible(false);
							splitPane.setDividerLocation(MainWindow.windowWidth - 10);
						}
					}
					
				});
		showActive.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(splitPane.getLeftComponent().isVisible() == false)
				{
					splitPane.getLeftComponent().setVisible(true);
					splitPane.setDividerLocation(MainWindow.windowWidth/2 - 20);
				}
				else
				{	
					splitPane.getLeftComponent().setVisible(false);
					splitPane.setDividerLocation(MainWindow.windowWidth - 10);
				}
			}
		});
		
		submit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				Doctor d1 = activeDoctorList.getSelectedValue();
				Doctor d2 = removedDoctorList.getSelectedValue();
				Doctor d = (d1 == null) ?  d2 : d1;
				if(d != null)
					changePanel.accept(new EditDoctor(sd, changePanel, d), "Edit Doctor");
			}
		});
	}

}
