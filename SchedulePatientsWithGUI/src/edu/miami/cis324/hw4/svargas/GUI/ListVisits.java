package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.BiConsumer;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.RowFilter;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import edu.miami.cis324.hw4.svargas.data.VisitComp;
import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;
import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.Visit;
import edu.miami.cis324.hw4.svargas.data.VisitImpl;

public class ListVisits extends JPanel {
	
	
	private class VisitTableModel extends AbstractTableModel{
		private static final long serialVersionUID = 1L;
		private SchedulerData schedulerData;
		private String[] columnNames = {"Date", "Time", "Patient", "Doctor"};
		private static final int COLUMN_DATE = 0;
		private static final int COLUMN_TIME = 1;
		private static final int COLUMN_PATIENT = 2;
		private static final int COLUMN_DOCTOR = 3;
		ArrayList<Visit<Integer,Integer>> vList;
		public VisitTableModel(SchedulerData sd)
		{
			schedulerData = sd;
			vList = new ArrayList<Visit<Integer, Integer>>(schedulerData.getVisitList());
			Collections.sort((List<Visit<Integer,Integer>>)vList, new VisitComp<Integer, Integer>());
			for(int i = 0; i < vList.size(); i++) //remove all visits from list that should not be shown
			{
				Visit<Integer, Integer> v = vList.get(i);
				Patient p = schedulerData.findPatient(v.getVisitor());
				Doctor d = schedulerData.findDoctor(v.getHost());
				
				if(p.isRemoved() == true || d.isRemoved() == true)
					vList.remove(i);
			}
		}
		
		/*public SchedulerData removed(SchedulerData sd)
		{
			SchedulerData newSD = sd;
			for(Visit<Integer, Integer> v : newSD.getVisitList())
			{
				if(newSD.findDoctor(v.getHost()).isRemoved() == true)
					newSD.getVisitList().remove(v);
				else if(newSD.findPatient(v.getVisitor()).isRemoved() == true)
					newSD.getVisitList().remove(v);
			}
			return newSD;
		}*/
		public Visit<Integer, Integer> getVisit(int row)
		{
			return vList.get(row);
		}
		
		@Override
		public boolean isCellEditable(int row, int col)
		{
			if(col == COLUMN_DATE)
				return true;
			return false;
		}
		
		@Override
		public String getColumnName(int columnIndex)
		{
			return columnNames[columnIndex];
		}
		
		@Override 
		public int getColumnCount()
		{
			return 4;
		}
		
		@Override
		public int getRowCount()
		{
			return vList.size();
		}
		
		@Override
		public Object getValueAt(int rowIndex, int columnIndex)
		{
			
			ArrayList<Patient> pList = (ArrayList<Patient>)(schedulerData.getPatientList());
			ArrayList<Doctor> dList = (ArrayList<Doctor>)(schedulerData.getDoctorList());
			//Collections.sort((List<Visit<Integer,Integer>>)vList, new VisitComp<Integer, Integer>());
			
			Visit<Integer, Integer> v = vList.get(rowIndex);
			Patient p = schedulerData.findPatient(v.getVisitor());
			Doctor d = schedulerData.findDoctor(v.getHost());
			
			switch(columnIndex){
			case COLUMN_DATE:
				return v.getVisitDate();//MyUtils.convertDateToString(v.getVisitDate(), dateFormat);
			case COLUMN_TIME:
				return MyUtils.convertDateToString(v.getVisitDate(), timeFormat);
			case COLUMN_PATIENT:
				return p.toString();
			case COLUMN_DOCTOR: 
				return d.toString();
			}
			return null;
		}
		
		@Override
		   public Class<?> getColumnClass(int columnIndex){
		          switch (columnIndex){
		             case COLUMN_DATE:
		            	 return Date.class;
		             case COLUMN_TIME:
		            	 return String.class;
		             case COLUMN_PATIENT:
		               return Patient.class;
		             case COLUMN_DOCTOR:
		               return Doctor.class;
		             }
		             return null;
		      } 
	}
	
	public static String title = "List All Visits";
	private GroupLayout listLayout;
	private JLabel header = new JLabel("List of Visits");
	private JScrollPane scroller = new JScrollPane();
	private JTable visitTable;
	private String dateFormat = "MM/dd/yyyy";
	private String timeFormat = "hh:mm a";
	private Font headerFont = new Font("Sitka Heading", Font.BOLD, 18);
	private JCheckBox showPast, showUpcoming;
	private List<RowFilter<TableModel, Integer>> filters;
	/*private JComboBox sortBy;
	private JLabel sortByLabel = new JLabel("Sort by: ");
	private String[] options = {""};*/
	
	/*public Date editDateDialog()
	{
		Date d = null;
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 250, 150);
		
		return d;
	}*/
	
	/**
	 * Create the panel.
	 */
	public ListVisits(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {
		listLayout = new GroupLayout(this);
		listLayout.setAutoCreateGaps(true);
		listLayout.setAutoCreateContainerGaps(true);
		setLayout(listLayout);
		
		RowFilter<TableModel, Integer> pastFilter = RowFilter.dateFilter(RowFilter.ComparisonType.BEFORE, Calendar.getInstance().getTime());
		RowFilter<TableModel, Integer> upcomingFilter = RowFilter.dateFilter(RowFilter.ComparisonType.AFTER, Calendar.getInstance().getTime());
		
		header.setAlignmentX(0.5f);
		header.setFont(headerFont);
		
		showUpcoming = new JCheckBox("Upcoming");
		showUpcoming.setSelected(true);
		showPast = new JCheckBox("Past");
		
		visitTable = new JTable();
		visitTable.setAutoCreateRowSorter(true);
		TableModel model = new VisitTableModel(sd);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
		visitTable.setModel(model);
		sorter.setRowFilter(upcomingFilter);
		visitTable.setRowSorter(sorter);
		scroller.setViewportView(visitTable);
		
		//sortBy = new JComboBox();
		
		filters = new ArrayList<RowFilter<TableModel, Integer>>();
		filters.add(upcomingFilter);
		filters.add(pastFilter);
		RowFilter<TableModel, Integer> rf = RowFilter.orFilter(filters);
		
		listLayout.setHorizontalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(header)
					.addGroup(listLayout.createSequentialGroup()
							.addComponent(showPast)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
				                    20, 20)
							.addComponent(showUpcoming))
					.addComponent(scroller)));
		
		listLayout.setVerticalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(header))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(showPast)
						.addComponent(showUpcoming))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(scroller)));
		
		showPast.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(showPast.isSelected() == true)	
				{	
					if(showUpcoming.isSelected() == true)
						sorter.setRowFilter(rf);
					else
						sorter.setRowFilter(pastFilter);
				}
				else 
				{
					if(showUpcoming.isSelected() == true)
						sorter.setRowFilter(RowFilter.notFilter(pastFilter));
					else
						sorter.setRowFilter(RowFilter.notFilter(rf));
				}
					
			}
			
		});
		showUpcoming.addActionListener(new ActionListener()
		{
		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(showUpcoming.isSelected() == true)
				{
					if(showPast.isSelected() == true)
						sorter.setRowFilter(rf);
					else
						sorter.setRowFilter(upcomingFilter);
				}
				else 
				{
					if(showPast.isSelected() == true)
						sorter.setRowFilter(RowFilter.notFilter(upcomingFilter));
					else
						sorter.setRowFilter(RowFilter.notFilter(rf));
				}
			}
		});
	}

}
