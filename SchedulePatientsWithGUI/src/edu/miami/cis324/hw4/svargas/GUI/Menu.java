package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.function.BiConsumer;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import edu.miami.cis324.hw4.svargas.SchedulerData;

public class Menu {

	public static JMenuBar createMenu(SchedulerData sd, BiConsumer<JPanel, String> changePanel)
	{
		JMenuBar menuBar = new JMenuBar();
		JMenu homeMenu, patientMenu, doctorMenu, visitMenu;
		
		homeMenu = new JMenu("Home");
		JMenuItem homeItem = new JMenuItem("Homepage");
		homeMenu.add(homeItem);
		menuBar.add(homeMenu);
		
		patientMenu = new JMenu("Patient");
		patientMenu.setMnemonic(KeyEvent.VK_P);
		patientMenu.getAccessibleContext().setAccessibleDescription("Menu for Managing Patients");
		menuBar.add(patientMenu);
		
		JMenuItem addPatientItem = new JMenuItem("Add a Patient");
		addPatientItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
		addPatientItem.getAccessibleContext().setAccessibleDescription("Enter a new patient");
		patientMenu.add(addPatientItem);
		JMenuItem listPatientItem = new JMenuItem("Edit Patient");
		listPatientItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));
		listPatientItem.getAccessibleContext().setAccessibleDescription("Lists patients to be seen, edited, or removed");
		patientMenu.add(listPatientItem);
		
		doctorMenu = new JMenu("Doctor");
		doctorMenu.setMnemonic(KeyEvent.VK_D);
		doctorMenu.getAccessibleContext().setAccessibleDescription("Menu for Managing Doctors");
		menuBar.add(doctorMenu);
		
		JMenuItem addDoctorItem = new JMenuItem("Add a Doctor");
		addDoctorItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.ALT_MASK));
		addDoctorItem.getAccessibleContext().setAccessibleDescription("Enter a new doctor");
		doctorMenu.add(addDoctorItem);
		JMenuItem listDoctorItem = new JMenuItem("Edit Doctor");
		listDoctorItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.ALT_MASK));
		listDoctorItem.getAccessibleContext().setAccessibleDescription("Lists doctors to be seen, edited, or removed");
		doctorMenu.add(listDoctorItem);
		
		visitMenu = new JMenu("Visit");
		visitMenu.setMnemonic(KeyEvent.VK_V);
		visitMenu.getAccessibleContext().setAccessibleDescription("Menu for Managing Visits");
		menuBar.add(visitMenu);

		JMenuItem addVisitItem = new JMenuItem("Add a Visit");
		addVisitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_5, ActionEvent.ALT_MASK));
		addVisitItem.getAccessibleContext().setAccessibleDescription("Enter a new visit");
		visitMenu.add(addVisitItem);
		
		JMenuItem editVisitItem = new JMenuItem("Edit Visits");
		editVisitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.ALT_MASK));
		editVisitItem.getAccessibleContext().setAccessibleDescription("Lists past and upcoming visits to be edited or removed");
		visitMenu.add(editVisitItem);
		
		JMenuItem listVisitItem = new JMenuItem("List All Visits");
		listVisitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_7, ActionEvent.ALT_MASK));
		listVisitItem.getAccessibleContext().setAccessibleDescription("Lists past and upcoming visits");
		visitMenu.add(listVisitItem);
		
		
		addPatientItem.addActionListener(event -> changePanel.accept(new AddPatient(sd, changePanel), AddPatient.title));
		addDoctorItem.addActionListener(event -> changePanel.accept(new AddDoctor(sd, changePanel), AddDoctor.title));
		addVisitItem.addActionListener(event -> changePanel.accept(new AddVisit(sd, changePanel), AddVisit.title));
		listPatientItem.addActionListener(event -> changePanel.accept(new ListPatients(sd, changePanel), ListPatients.title));
		listDoctorItem.addActionListener(event -> changePanel.accept(new ListDoctors(sd, changePanel), ListDoctors.title));
		editVisitItem.addActionListener(event -> changePanel.accept(new EditVisit(sd, changePanel), EditVisit.title));
		listVisitItem.addActionListener(event -> changePanel.accept(new ListVisits(sd, changePanel), ListVisits.title));
		homeItem.addActionListener(event -> changePanel.accept(new Homepage(sd, changePanel), "Homepage"));
		return menuBar;
	}
	
	public static void showDialog(String message)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 200, 100);
		JOptionPane.showMessageDialog(frame, message, "Message", JOptionPane.PLAIN_MESSAGE);
	}
	
	public static int showConfirmationDialog(String message)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 200, 100);
		int n = JOptionPane.showConfirmDialog(
			    frame,
			    message,
			    "Confirm",
			    JOptionPane.YES_NO_OPTION);
		return n;
	}
	
	public static void showErrorDialog(String message)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 200, 100);
		JOptionPane.showMessageDialog(frame, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
}
