package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.BiConsumer;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;
import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.MedicalSpecialty;

public class EditDoctor extends JPanel {

	public static String title = "Edit Doctor Form";
	private GroupLayout formLayout;
	private JLabel header = new JLabel(title);
	private JLabel firstNameLabel = new JLabel();
	private JLabel middleInitialLabel = new JLabel();
	private JLabel lastNameLabel = new JLabel();
	private JLabel ssnLabel = new JLabel();
	private JLabel dobLabel = new JLabel();
	private JLabel specialtyLabel = new JLabel();
	private JTextField firstNameText  = new JTextField();
	private JTextField lastNameText  = new JTextField();
	private JTextField middleInitialText  = new JTextField();
	private JTextField ssnText  = new JTextField();
	private JTextField dobText  = new JTextField();
	private JComboBox<MedicalSpecialty> specialties = new JComboBox<MedicalSpecialty>(MedicalSpecialty.values());
	private JCheckBox remove = new JCheckBox();
	private JButton submitBtn = new JButton("SUBMIT");

	private MedicalSpecialty specialty;
	private static String format = "MM/dd/yyyy";
	/**
	 * Create the panel.
	 */
	public EditDoctor(SchedulerData sd, BiConsumer<JPanel, String> changePanel, Doctor doc) {
		formLayout = new GroupLayout(this);
		setLayout(formLayout);
		//setContentPane(contentPane);
		
		formLayout.setAutoCreateGaps(true);
		formLayout.setAutoCreateContainerGaps(true);
		
		header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setAlignmentX(0.5f);
		header.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		
		firstNameLabel.setText("First Name");
		middleInitialLabel.setText("Middle Initial");
		lastNameLabel.setText("Last Name");
		ssnLabel.setText("Social Security Number");
		dobLabel.setText("Date of Birth");
		specialtyLabel.setText("Medical Specialty");
		middleInitialText.setMaximumSize(new Dimension(30, middleInitialText.getHeight()));
		
		remove.setText("Remove?");

		if(doc.isRemoved() == true)
			remove.setSelected(true);
		else
			remove.setSelected(false);
		
		firstNameText.setText(doc.getFirstName());
		lastNameText.setText(doc.getLastName());
		ssnText.setText(doc.getSSN());
		dobText.setText(MyUtils.convertDateToString(doc.getDOB(), format));
		specialties.setSelectedItem(doc.getSpecialty());
		
		submitBtn.setForeground(Color.BLACK);
		submitBtn.setBackground(Color.GREEN);
		submitBtn.setPreferredSize(new Dimension(125, 30));
		
		//setJMenuBar(Menu.createMenu());		
		
		formLayout.setHorizontalGroup(
				formLayout.createSequentialGroup()
				.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(formLayout.createSequentialGroup()
								.addComponent(header)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
					                     GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(remove))
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameText)))
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(ssnLabel)
										.addComponent(dobLabel)
										.addComponent(specialtyLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(ssnText)
										.addComponent(dobText)
										.addComponent(specialties)
										.addComponent(submitBtn)))));
		
		formLayout.setVerticalGroup(
				formLayout.createSequentialGroup()
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(header)
						.addComponent(remove))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(firstNameLabel)
						.addComponent(firstNameText)
						.addComponent(middleInitialLabel)
						.addComponent(middleInitialText)
						.addComponent(lastNameLabel)
						.addComponent(lastNameText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(ssnLabel)
						.addComponent(ssnText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(dobLabel)
						.addComponent(dobText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(specialtyLabel)
						.addComponent(specialties))
				.addComponent(submitBtn));
		
		specialties.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				specialty = (MedicalSpecialty) specialties.getSelectedItem();
			}
		});
		submitBtn.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String name = firstNameText.getText() + " " + lastNameText.getText();
				doc.setFullName(name);
				doc.setPersonalData(ssnText.getText(), MyUtils.convertStringToDate(dobText.getText(), format));
				doc.setSpecialty(specialty);
				Menu.showDialog("Patient " + name + " has been successfully modified!");
				changePanel.accept(new Homepage(sd, changePanel), Homepage.title);
			}
		});
		
		remove.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				if(remove.isSelected() == true)
				{
					String name = doc.getFirstName() + " " + doc.getLastName() ;
					int result = Menu.showConfirmationDialog("Are you sure you wish to remove " + name + "?");
					if(result == JOptionPane.YES_OPTION)
					{	
						remove.setSelected(true);
						doc.markAsRemoved();
						Menu.showDialog(name + " has been marked as removed");
					}
					else
						remove.setSelected(false);
				}
				else
				{
					String name = doc.getFirstName() + " " + doc.getLastName() ;
					int result = Menu.showConfirmationDialog("Are you sure you wish to unremove " + name  + "?");
					if(result == JOptionPane.YES_OPTION)
					{	
						remove.setSelected(false);
						doc.unRemove();
					}
					else
						remove.setSelected(true);
				}
			}
		});
	}

}
