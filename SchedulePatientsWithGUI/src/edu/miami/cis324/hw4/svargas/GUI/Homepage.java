package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.function.BiConsumer;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import edu.miami.cis324.hw4.svargas.SchedulerData;

import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;

public class Homepage extends JPanel {
	
	//private JPanel contentPane;
	private String welcome = "Welcome!";
	public static String title = "Homepage";
	private int windowHeight = 250;
	private int windowWidth = 500;
	JLabel label = new JLabel(welcome);
	JButton addPatient = new JButton("Add Patient");
	JButton addDoctorBtn = new JButton("Add Doctor");
	JButton addVisitBtn = new JButton("Add Visit");
	JButton listPatientBtn = new JButton("Edit Patient");
	JButton listDoctorBtn = new JButton("Edit Doctor");
	JButton editVisitBtn = new JButton("View/Edit Visits");
	JButton listVisitBtn = new JButton("List Visits");
	
	//public static String title = "Patient Scheduling System";
	
	/**
	 * Launch the application.
	 *
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Homepage frame = new Homepage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Homepage(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {
		/*super("Patient Scheduling System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, windowWidth, windowHeight);
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		setContentPane(contentPane);
		//JPanel contentPane = new JPanel();
		
		//contentPane.setPreferredSize(new Dimension(windowWidth, windowHeight));
		//getContentPane().add(contentPane);*/
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		Box labelBox = Box.createVerticalBox();
		Font f = new Font("Arial", 10, 12);
		
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setAlignmentX(0.5f);
		label.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		labelBox.add(label);
		add(labelBox);
		
		add(Box.createRigidArea(new Dimension(0, windowHeight/12)));
		//contentPane.add(labelBox);
		//contentPane.add(label);	
		
		//setJMenuBar(Menu.createMenu());

		Box buttonRow1 = Box.createHorizontalBox();
		buttonRow1.setPreferredSize(new Dimension(getWidth(), getHeight()/8));
		JPanel buttonPane = new JPanel();
		
		buttonRow1.setBorder(new EmptyBorder(getHeight()/10,0,10,0));
		
		
		addPatient.setMaximumSize(new Dimension(100, 30));
		addPatient.setPreferredSize(new Dimension(125, 30));
		addPatient.setForeground(Color.BLACK);
		addPatient.setBackground(new Color(20,250,40));
		addPatient.addActionListener(event -> {changePanel.accept(new AddPatient(sd, changePanel), AddPatient.title);});	
		buttonPane.add(addPatient);
		buttonPane.add(Box.createRigidArea(new Dimension(windowWidth/15,0)));
		
		
		addDoctorBtn.setForeground(Color.BLACK);
		addDoctorBtn.setBackground(Color.GREEN);
		addDoctorBtn.setPreferredSize(new Dimension(125, 30));
		addDoctorBtn.addActionListener(event -> {changePanel.accept(new AddDoctor(sd, changePanel), AddDoctor.title);});
		buttonPane.add(addDoctorBtn);
		buttonPane.add(Box.createRigidArea(new Dimension(windowWidth/15,0)));
		
		//JButton addVisitBtn = new JButton("Add Visit");
		addVisitBtn.setBackground(Color.GREEN);
		addVisitBtn.setForeground(Color.BLACK);
		addVisitBtn.setPreferredSize(new Dimension(125, 30));
		addVisitBtn.addActionListener(event -> {changePanel.accept(new AddVisit(sd, changePanel), AddVisit.title);});
		buttonPane.add(addVisitBtn);
		buttonPane.add(Box.createRigidArea(new Dimension(windowWidth/15,0)));
		
		buttonRow1.add(buttonPane);
		add(buttonRow1);
		
		
		Box buttonRow2 = Box.createHorizontalBox();
		buttonRow2.setPreferredSize(new Dimension(getWidth(), getHeight()/8));
		JPanel lowerButtonPane = new JPanel();
		
		listPatientBtn.getAccessibleContext().setAccessibleDescription("Lists patients to be seen, edited, or removed");
		listPatientBtn.setBackground(Color.GREEN);
		listPatientBtn.setForeground(Color.BLACK);
		listPatientBtn.setPreferredSize(new Dimension(125, 30));
		listPatientBtn.addActionListener(event -> {changePanel.accept(new ListPatients(sd, changePanel), ListPatients.title);});
		lowerButtonPane.add(listPatientBtn);
		lowerButtonPane.add(Box.createRigidArea(new Dimension(windowWidth/15,0)));
		
		
		listDoctorBtn.getAccessibleContext().setAccessibleDescription("Lists doctors to be seen, edited, or removed");
		listDoctorBtn.setBackground(Color.GREEN);
		listDoctorBtn.setForeground(Color.BLACK);
		listDoctorBtn.setPreferredSize(new Dimension(125, 30));
		listDoctorBtn.addActionListener(event -> {changePanel.accept(new ListDoctors(sd, changePanel), ListDoctors.title);});
		lowerButtonPane.add(listDoctorBtn);
		lowerButtonPane.add(Box.createRigidArea(new Dimension(windowWidth/15,0)));
		
		
		editVisitBtn.getAccessibleContext().setAccessibleDescription("Upcoming visits to be edited or removed");
		editVisitBtn.setBackground(Color.GREEN);
		editVisitBtn.setForeground(Color.BLACK);
		editVisitBtn.setPreferredSize(new Dimension(125, 30));
		editVisitBtn.addActionListener(event -> {changePanel.accept(new EditVisit(sd, changePanel), EditVisit.title);});
		lowerButtonPane.add(editVisitBtn);
		lowerButtonPane.add(Box.createRigidArea(new Dimension(windowWidth/15,0)));
		buttonRow2.add(lowerButtonPane);
		add(buttonRow2);
		
		JPanel thirdButtonPane = new JPanel();
		
		Box buttonRow3 = Box.createHorizontalBox();
		buttonRow3.setPreferredSize(new Dimension(getWidth(), getHeight()/8));
		listVisitBtn.setBackground(Color.GREEN);
		listVisitBtn.setForeground(Color.BLACK);
		listVisitBtn.setPreferredSize(new Dimension(125, 30));
		listVisitBtn.addActionListener(event -> {changePanel.accept(new ListVisits(sd, changePanel), ListVisits.title);});
		thirdButtonPane.add(listVisitBtn);
		
		buttonRow3.add(thirdButtonPane);
		add(buttonRow3);
	}
	
	public void setButtonAction(ActionListener listener)
	{
		addPatient.addActionListener(listener);
	}

}