package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.function.BiConsumer;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.data.Patient;

public class ListPatients extends JPanel {

	public static String title = "Edit Patient";
	private GroupLayout listLayout;
	Font activeFont = new Font("Arial", Font.PLAIN, 14);
	Font removedFont = new Font("Arial", Font.ITALIC, 14);
	private JLabel header = new JLabel("Choose Patient to Edit");
	private JScrollPane removedScroller;
	private JScrollPane activeScroller;
	private JSplitPane splitPane;
	private JCheckBox showActive, showRemoved;
	private JButton submit;
	
	/**
	 * Create the panel.
	 */
	public ListPatients(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {
		listLayout = new GroupLayout(this);
		listLayout.setAutoCreateGaps(true);
		listLayout.setAutoCreateContainerGaps(true);
		setLayout(listLayout);
		
		header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setAlignmentX(0.5f);
		header.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		
		removedScroller = new JScrollPane();
		activeScroller = new JScrollPane();
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, activeScroller, removedScroller);
		splitPane.setOneTouchExpandable(true);
		splitPane.getRightComponent().setVisible(false);
		
		JList<Patient> activePatientList = new JList<Patient>();
		JList<Patient> removedPatientList = new JList<Patient>();
		
		showActive = new JCheckBox("Active");
		showActive.setSelected(true);
		showRemoved = new JCheckBox("Removed");
		showRemoved.setAlignmentX(RIGHT_ALIGNMENT);
		
		submit = new JButton("Edit Patient");
		submit.setBackground(Color.GREEN);
		
		Collection<Patient> pList = sd.getPatientList();
		DefaultListModel<Patient> activeDLM = new DefaultListModel<Patient>();
		DefaultListModel<Patient> removedDLM = new DefaultListModel<Patient>();
		for(Patient p : pList){
			if(p.isRemoved() == true)
				removedDLM.addElement(p);
			else
				activeDLM.addElement(p);
		}
		activePatientList.setFont(activeFont);
		activePatientList.setModel(activeDLM);
		activePatientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		removedPatientList.setFont(removedFont);
		removedPatientList.setModel(removedDLM);
		removedPatientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		activeScroller.setViewportView(activePatientList);
		removedScroller.setViewportView(removedPatientList);
		
		listLayout.setHorizontalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(header)
					.addGroup(listLayout.createSequentialGroup()
							.addComponent(showActive)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
				                     GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(showRemoved))
					.addComponent(splitPane)
					.addComponent(submit)));
		
		listLayout.setVerticalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(header))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(showActive)
						.addComponent(showRemoved))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(splitPane))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(submit)));
		showRemoved.addActionListener(new ActionListener()
				{

					@Override
					public void actionPerformed(ActionEvent arg0) {
						if(splitPane.getRightComponent().isVisible() == false)
						{
							splitPane.getRightComponent().setVisible(true);
							splitPane.setDividerLocation(MainWindow.windowWidth/2 - MainWindow.windowWidth/25);
						}
						else
						{	
							splitPane.getRightComponent().setVisible(false);
							splitPane.setDividerLocation(MainWindow.windowWidth - MainWindow.windowWidth/50);
						}
					}
					
				});
		showActive.addActionListener(new ActionListener() //action for when 'Active' checkbox pressed
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(splitPane.getLeftComponent().isVisible() == false)
				{
					splitPane.getLeftComponent().setVisible(true);
					splitPane.setDividerLocation(MainWindow.windowWidth/2 - 20);
				}
				else
				{	
					splitPane.getLeftComponent().setVisible(false);
					splitPane.setDividerLocation(MainWindow.windowWidth - 10);
				}
			}
		});
		
		submit.addActionListener(new ActionListener() //actions for when the 'submit' button is pressed
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				Patient p1 = activePatientList.getSelectedValue();
				Patient p2 = removedPatientList.getSelectedValue();
				Patient p = (p1 == null) ?  p2 : p1; //sees if the patient selected is from the active list or removed list
				if(p != null) //changes panels if a patient is selected when submit button is pressed
					changePanel.accept(new EditPatient(sd, changePanel, p), "Edit Patient");
			}
		});
	}
}
