package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;
import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.PatientImpl;
import edu.miami.cis324.hw4.svargas.data.Visit;
import edu.miami.cis324.hw4.svargas.data.VisitImpl;

public class AddVisit extends JPanel {

	public static String title = "Enter New Visit";
	private GroupLayout listLayout;
	private JLabel header = new JLabel(title);
	private JScrollPane patientScroller;
	private JScrollPane doctorScroller;
	private JSplitPane splitPane;
	private JButton submit;
	private JLabel dateLabel = new JLabel("Date");
	private JLabel timeLabel = new JLabel("Time");
	private JLabel patientLabel = new JLabel("Choose Patient");
	private JLabel doctorLabel = new JLabel("Choose Doctor");
	private JComboBox<String> month;
	private JComboBox<Integer> day;
	private JComboBox<Integer> hour;
	private JComboBox<Integer> minute;
	private JComboBox<String> ampm;
	private JComboBox<Integer> year;
	private static String format = "MMMM dd, yyyy HH:mm";
	private String list[] = {"January", "February", "March", "April", "May", "June", "July",
			"August", "September", "October", "November", "December"};
	private String ampmOptions[] = {"AM", "PM"};
	private Integer hourOptions[] = IntStream.rangeClosed(1,12).boxed().toArray(Integer[]::new);
	private Integer minuteOptions[] = IntStream.rangeClosed(01,60).boxed().toArray(Integer[]::new);
	private int daysPerMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	private Integer yearOptions[] = IntStream.rangeClosed(1950,2050).boxed().toArray(Integer[]::new);
	
	/**
	 * Create the panel.
	 */
	public AddVisit(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {
		listLayout = new GroupLayout(this);
		listLayout.setAutoCreateGaps(true);
		listLayout.setAutoCreateContainerGaps(true);
		setLayout(listLayout);
		
		DefaultComboBoxModel<Integer> dayModel = new DefaultComboBoxModel<Integer>();
		for(int i = 1; i <= daysPerMonth[0]; i++)
		{
			dayModel.addElement(Integer.valueOf(i));
		}
					
		
		int calendarYear = Calendar.getInstance().YEAR;
		if(calendarYear % 4 == 0 && (calendarYear % 100 != 0 || calendarYear % 400 == 0))
			daysPerMonth[1] = 29;
		//dayOptions = IntStream.of(daysPerMonth).boxed().toArray(Integer[]::new);
		//dayOptions = IntStream.rangeClosed(1,31).boxed().toArray(Integer[]::new);
		
		
		month = new JComboBox<String>(list);
		ampm = new JComboBox<String>(ampmOptions);
		hour = new JComboBox<Integer>(hourOptions);
		minute = new JComboBox<Integer>(minuteOptions);
		day = new JComboBox<Integer>();
		day.setModel(dayModel);
		year = new JComboBox<Integer>(yearOptions);
		
		
		//header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setAlignmentX(0.5f);
		header.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		
		patientScroller = new JScrollPane();
		doctorScroller = new JScrollPane();
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, patientScroller, doctorScroller);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation((int)(MainWindow.windowWidth/2 - MainWindow.windowWidth/12.5));
		
		JList<Patient> patientList = new JList<Patient>();
		JList<Doctor> doctorList = new JList<Doctor>();
		
		submit = new JButton("Add Visit");
		submit.setBackground(Color.GREEN);
		
		Collection<Patient> pList = sd.getPatientList();
		Collection<Doctor> dList = sd.getDoctorList();
		DefaultListModel<Patient> patientDLM = new DefaultListModel<Patient>();
		DefaultListModel<Doctor> doctorDLM = new DefaultListModel<Doctor>();
		for(Patient p : pList){
			if(p.isRemoved() == false)
				patientDLM.addElement(p);
		}
		for(Doctor d: dList)
		{
			if(d.isRemoved() == false)
				doctorDLM.addElement(d);
		}
		patientList.setModel(patientDLM);
		patientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		doctorList.setModel(doctorDLM);
		doctorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		patientScroller.setViewportView(patientList);
		doctorScroller.setViewportView(doctorList);
		
		listLayout.setHorizontalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(header)
					
					.addGroup(listLayout.createSequentialGroup()
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(dateLabel))
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(month))
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(day))
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(year))
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(timeLabel))
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(hour))
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(minute))
							.addGroup(listLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(ampm)))
					.addGroup(listLayout.createSequentialGroup()
							.addComponent(patientLabel)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
				                     GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(doctorLabel))
					.addComponent(splitPane)
					.addComponent(submit)));
		
		listLayout.setVerticalGroup(listLayout.createSequentialGroup()
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(header))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(dateLabel)
						.addComponent(month)
						.addComponent(day)
						.addComponent(year)
						.addComponent(timeLabel)
						.addComponent(hour)
						.addComponent(minute)
						.addComponent(ampm))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(patientLabel)
						.addComponent(doctorLabel))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(splitPane))
				.addGroup(listLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(submit)));
		
		month.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						int index = month.getSelectedIndex();
						int dayCount = daysPerMonth[index];
						int currentDayCount = dayModel.getSize();
						if(currentDayCount < dayCount)
						{
							while(currentDayCount < dayCount)
							{
								currentDayCount++;
								dayModel.addElement(Integer.valueOf(currentDayCount));
							}
						}
						else if(currentDayCount > dayCount)
						{
							while(currentDayCount > dayCount)
							{
								currentDayCount--;
								dayModel.removeElementAt(currentDayCount);
							}
						}
						
					}
				});
		
		year.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						int yearSelected = (Integer)year.getSelectedItem();
						int currentDayCount = dayModel.getSize();
						if(yearSelected % 4 == 0 && (yearSelected % 100 != 0 || yearSelected % 400 == 0))
						{	
							daysPerMonth[1] = 29;
							if(currentDayCount == 28)
								dayModel.addElement(daysPerMonth[1]);
						}
						else
						{
							daysPerMonth[1] = 28;
							if(currentDayCount == 29)
								dayModel.removeElement(29);
						}
						
					}
				});
		
		submit.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int result = Menu.showConfirmationDialog("Are you sure you wish to add this visit?");
				if(result == JOptionPane.YES_OPTION)
				{
					int result1 = patientList.getSelectedIndex();
					int result2 = doctorList.getSelectedIndex();
					if(result1 == -1 || result2 == -1)
						Menu.showErrorDialog("One patient and One Doctor must be selected");
					else{
						Patient p = patientList.getSelectedValue();
						Doctor d = doctorList.getSelectedValue();
						int hourSelected = (int)hour.getSelectedItem();
						if(ampm.getSelectedItem().equals("PM"))
							hourSelected += 12;
						String selectedDate = month.getSelectedItem() + " " + (day.getSelectedIndex() + 1) + ", " + year.getSelectedItem()
						  					+ " " + hourSelected + ":" + minute.getSelectedItem();
						Visit<Integer, Integer> v = new VisitImpl<Integer, Integer>
								(p.getPatientID(), d.getDoctorID(), MyUtils.convertStringToDate(selectedDate, format));
						sd.addVisit(v);
						Menu.showDialog("Visit has been successfully added!");
						changePanel.accept(new Homepage(sd, changePanel), Homepage.title);
					}
				}
				
			}
		});
	}

}
