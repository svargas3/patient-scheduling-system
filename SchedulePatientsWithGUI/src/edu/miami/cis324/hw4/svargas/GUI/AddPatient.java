package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.BiConsumer;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.PatientImpl;

public class AddPatient extends JPanel {
	
	public static String title = "Patient Registration Form";
	private GroupLayout formLayout;
	private JLabel header = new JLabel(title);
	private JLabel firstNameLabel = new JLabel();
	private JLabel middleInitialLabel = new JLabel();
	private JLabel lastNameLabel = new JLabel();
	private JLabel ssnLabel = new JLabel();
	private JLabel dobLabel = new JLabel();
	private JTextField firstNameText  = new JTextField();
	private JTextField lastNameText  = new JTextField();
	private JTextField middleInitialText  = new JTextField();
	private JTextField ssnText  = new JTextField();
	private JTextField dobText  = new JTextField();
	private JButton submitBtn = new JButton("SUBMIT");
	private Patient pat;
	private static String format = "MM/dd/yyyy";
	/**
	 * Launch the application.
	 *
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddPatient frame = new AddPatient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public AddPatient(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {
		formLayout = new GroupLayout(this);
	    setLayout(formLayout);
		
		formLayout.setAutoCreateGaps(true);
		formLayout.setAutoCreateContainerGaps(true);
		
		header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setAlignmentX(0.5f);
		header.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		
		DatePicker datePicker = new DatePicker();
		GroupLayout groupLayout = (GroupLayout) datePicker.getLayout();
		firstNameLabel.setText("First Name:");
		middleInitialLabel.setText("Middle Initial");
		lastNameLabel.setText("Last Name: ");
		ssnLabel.setText("Social Security Number: ");
		dobLabel.setText("Date of Birth");
		middleInitialText.setMaximumSize(new Dimension(30, middleInitialText.getHeight()));
		
		submitBtn.setForeground(Color.BLACK);
		submitBtn.setBackground(Color.GREEN);
		submitBtn.setPreferredSize(new Dimension(125, 30));		
		
		formLayout.setHorizontalGroup(
				formLayout.createSequentialGroup()
				.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(header)
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameText)))
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(ssnLabel)
										.addComponent(dobLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(ssnText)
										.addComponent(datePicker)
										//.addComponent(dobText)
										.addComponent(submitBtn)))));
		
		formLayout.setVerticalGroup(
				formLayout.createSequentialGroup()
				.addComponent(header)
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(firstNameLabel)
						.addComponent(firstNameText)
						.addComponent(middleInitialLabel)
						.addComponent(middleInitialText)
						.addComponent(lastNameLabel)
						.addComponent(lastNameText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(ssnLabel)
						.addComponent(ssnText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(dobLabel)
						.addComponent(datePicker))
						//.addComponent(dobText))
				.addComponent(submitBtn));
		
		submitBtn.addActionListener(new ActionListener()
				{

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						String name = firstNameText.getText() + " " + lastNameText.getText();
						int result = Menu.showConfirmationDialog("Are you sure you wish to add " + name + "?");
						if(result == JOptionPane.YES_OPTION)
						{
							//pat = new PatientImpl(name, ssnText.getText(), MyUtils.convertStringToDate(dobText.getText(), format));
							pat = new PatientImpl(name, ssnText.getText(), datePicker.getDate());
							sd.addPatient(pat);
							Menu.showDialog("Patient " + name + " has been successfully added!");
							changePanel.accept(new Homepage(sd, changePanel), Homepage.title);
						}
						
					}
				});
	}
}
