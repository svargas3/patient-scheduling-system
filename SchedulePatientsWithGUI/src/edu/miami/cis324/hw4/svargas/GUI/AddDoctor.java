package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.BiConsumer;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;
import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.DoctorImpl;
import edu.miami.cis324.hw4.svargas.data.MedicalSpecialty;
import edu.miami.cis324.hw4.svargas.data.PatientImpl;

public class AddDoctor extends JPanel {

	
	//private int windowHeight = 250;
	//private int windowWidth = 500;
	public static String title = "Doctor Registration Form";
	private GroupLayout formLayout;
	private JLabel header = new JLabel("Doctor Registration Form");
	private JLabel firstNameLabel = new JLabel();
	private JLabel middleInitialLabel = new JLabel();
	private JLabel lastNameLabel = new JLabel();
	private JLabel ssnLabel = new JLabel();
	private JLabel dobLabel = new JLabel();
	private JLabel specialtyLabel = new JLabel();
	private JTextField firstNameText  = new JTextField();
	private JTextField lastNameText  = new JTextField();
	private JTextField middleInitialText  = new JTextField();
	private JTextField ssnText  = new JTextField();
	private JTextField dobText  = new JTextField();
	private JComboBox<MedicalSpecialty> specialties = new JComboBox<MedicalSpecialty>(MedicalSpecialty.values());
	private JButton submitBtn = new JButton("SUBMIT");
	private Doctor doc;
	private MedicalSpecialty specialty;
	private static String format = "MM/dd/yyyy";

	/**
	 * Create the frame.
	 */
	public AddDoctor(SchedulerData sd, BiConsumer<JPanel, String> changePanel) {
		/*super("Doctor Registration Form");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, windowWidth, windowHeight);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));*/
		formLayout = new GroupLayout(this);
		setLayout(formLayout);
		//setContentPane(contentPane);
		
		formLayout.setAutoCreateGaps(true);
		formLayout.setAutoCreateContainerGaps(true);
		
		header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setAlignmentX(0.5f);
		header.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		
		firstNameLabel.setText("First Name");
		middleInitialLabel.setText("Middle Initial");
		lastNameLabel.setText("Last Name");
		ssnLabel.setText("Social Security Number");
		dobLabel.setText("Date of Birth");
		specialtyLabel.setText("Medical Specialty");
		middleInitialText.setMaximumSize(new Dimension(30, middleInitialText.getHeight()));
		
		specialties.setSelectedIndex(-1);
		
		submitBtn.setForeground(Color.BLACK);
		submitBtn.setBackground(Color.GREEN);
		submitBtn.setPreferredSize(new Dimension(125, 30));
		
		DatePicker datePicker = new DatePicker();
		//setJMenuBar(Menu.createMenu());		
		
		formLayout.setHorizontalGroup(
				formLayout.createSequentialGroup()
				.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(header)
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameText)))
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(ssnLabel)
										.addComponent(dobLabel)
										.addComponent(specialtyLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(ssnText)
										//.addComponent(dobText)
										.addComponent(datePicker)
										.addComponent(specialties)
										.addComponent(submitBtn)))));
		
		formLayout.setVerticalGroup(
				formLayout.createSequentialGroup()
				.addComponent(header)
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(firstNameLabel)
						.addComponent(firstNameText)
						.addComponent(middleInitialLabel)
						.addComponent(middleInitialText)
						.addComponent(lastNameLabel)
						.addComponent(lastNameText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(ssnLabel)
						.addComponent(ssnText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(dobLabel)
						.addComponent(datePicker))
						//.addComponent(dobText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(specialtyLabel)
						.addComponent(specialties))
				.addComponent(submitBtn));
		
		specialties.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				specialty = (MedicalSpecialty) specialties.getSelectedItem();
			}
		});
		submitBtn.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				String name = firstNameText.getText() + " " + lastNameText.getText();
				int result = Menu.showConfirmationDialog("Are you sure you wish to add " + name + "?");
				if(result == JOptionPane.YES_OPTION)
				{	
					//doc = new DoctorImpl(name, ssnText.getText(), MyUtils.convertStringToDate(dobText.getText(), format), specialty);
					doc = new DoctorImpl(name, ssnText.getText(), datePicker.getDate(), specialty);
					sd.addDoctor(doc);
					Menu.showDialog("Doctor " + name + " has been successfully added!");
					changePanel.accept(new Homepage(sd, changePanel), Homepage.title);
				}

			}
		});
	}
}
