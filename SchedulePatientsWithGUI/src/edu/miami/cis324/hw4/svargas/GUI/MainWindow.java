package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.xml.stream.XMLStreamException;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.SchedulerXMLReaderUtils;
import edu.miami.cis324.hw4.svargas.Utilities.SchedulerXMLWriterUtils;
import edu.miami.cis324.hw4.svargas.data.DoctorImpl;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.PatientImpl;

public class MainWindow extends JFrame {
	
	private SchedulerData sd;
	private final static String INPUT_FILE = "resources\\schedulerData.xml";
	private final static String OUTPUT_FILE = "resources\\schedulerOut.xml";
	class ButtonAction implements ActionListener{
		private JPanel panel;
		private String title;
		
		public ButtonAction(JPanel pnl, String name)
		{
			panel = pnl;
			title = name;
		}
		
		public void setButtonAction(JPanel pnl, String name)
		{
			panel = pnl;
			title = name;
		}
		@Override
		public void actionPerformed(ActionEvent e)
		{
			changePanel(panel, title);
		}
	}
	public static int windowHeight = 300;
	public static int windowWidth = 500;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					frame.addWindowListener(new WindowAdapter(){
						public void windowClosing(WindowEvent e)
						{
							try {
								SchedulerXMLWriterUtils.writeSchedulerXML(OUTPUT_FILE, frame.sd);
							} catch (IOException | XMLStreamException e1) {
								e1.printStackTrace();
							}
							frame.dispose();
							System.exit(0);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	public void changePanel(JPanel panel, String name) {
	    //System.out.println(" name: " + name);
		getContentPane().removeAll();
	    setContentPane(panel);
	    getContentPane().revalidate();
	    getContentPane().repaint();
	    setTitle(name);
	    //update(getGraphics());
	}
	
	/**
	 * Create the frame.
	 * @throws XMLStreamException 
	 * @throws FileNotFoundException 
	 */
	public MainWindow() throws FileNotFoundException, XMLStreamException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sd = SchedulerXMLReaderUtils.readSchedulingXML(OUTPUT_FILE);
		ArrayList<Patient> ps = (ArrayList<Patient>) sd.getPatientList();
		ps.get(1).markAsRemoved();
		int maxDoc = sd.getMaxDocIndex();
		int maxPat = sd.getMaxPatIndex();
		PatientImpl.setIDcounter(maxPat + 1);
		DoctorImpl.setIDCounter(maxDoc + 1);
		
		setBounds(100, 100, windowWidth, windowHeight);
		setJMenuBar(Menu.createMenu(sd, (panel, title) -> {changePanel(panel, title);}));
		Homepage homepage = new Homepage(sd, (panel, title) -> {changePanel(panel, title);});
		setTitle(Homepage.title);
		changePanel(homepage, Homepage.title);
	}
}
