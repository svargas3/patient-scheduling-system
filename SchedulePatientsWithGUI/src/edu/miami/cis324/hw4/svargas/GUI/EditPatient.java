package edu.miami.cis324.hw4.svargas.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.BiConsumer;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.Utilities.MyUtils;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.PatientImpl;
import edu.miami.cis324.hw4.svargas.data.PersonalData;

public class EditPatient extends JPanel {

	public static String title = "Edit Patient Form";
	private GroupLayout formLayout;
	private JLabel header = new JLabel(title);
	private JLabel firstNameLabel = new JLabel();
	private JLabel middleInitialLabel = new JLabel();
	private JLabel lastNameLabel = new JLabel();
	private JLabel ssnLabel = new JLabel();
	private JLabel dobLabel = new JLabel();
	private JTextField firstNameText  = new JTextField();
	private JTextField lastNameText  = new JTextField();
	private JTextField middleInitialText  = new JTextField();
	private JTextField ssnText  = new JTextField();
	private JTextField dobText  = new JTextField();
	private JCheckBox remove = new JCheckBox();
	private JButton submitBtn = new JButton("SUBMIT");
	private static String format = "MM/dd/yyyy";
	/**
	 * Create the panel.
	 */
	public EditPatient(SchedulerData sd, BiConsumer<JPanel, String> changePanel, Patient patient) {
		formLayout = new GroupLayout(this);
	    setLayout(formLayout);
		
		formLayout.setAutoCreateGaps(true);
		formLayout.setAutoCreateContainerGaps(true);
		
		header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setAlignmentX(0.5f);
		header.setFont(new Font("Sitka Heading", Font.BOLD, 18));
		
		firstNameLabel.setText("First Name:");
		middleInitialLabel.setText("Middle Initial");
		lastNameLabel.setText("Last Name: ");
		ssnLabel.setText("Social Security Number: ");
		dobLabel.setText("Date of Birth");
		middleInitialText.setMaximumSize(new Dimension(30, middleInitialText.getHeight()));
		
		firstNameText.setText(patient.getFirstName());
		lastNameText.setText(patient.getLastName());
		ssnText.setText(patient.getSSN());
		dobText.setText(MyUtils.convertDateToString(patient.getDOB(), format));
		
		remove.setText("Remove?");
		if(patient.isRemoved() == true)
			remove.setSelected(true);
		else
			remove.setSelected(false);
		
		submitBtn.setForeground(Color.BLACK);
		submitBtn.setBackground(Color.GREEN);
		submitBtn.setPreferredSize(new Dimension(125, 30));		
		
		formLayout.setHorizontalGroup(
				formLayout.createSequentialGroup()
				.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(formLayout.createSequentialGroup()
								.addComponent(header)
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
					                     GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(remove))
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(firstNameText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(middleInitialText))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lastNameText)))
						.addGroup(formLayout.createSequentialGroup()
								.addGroup(formLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(ssnLabel)
										.addComponent(dobLabel))
								.addGroup(formLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(ssnText)
										.addComponent(dobText)
										.addComponent(submitBtn)))));
		
		formLayout.setVerticalGroup(
				formLayout.createSequentialGroup()
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(header)
						.addComponent(remove))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(firstNameLabel)
						.addComponent(firstNameText)
						.addComponent(middleInitialLabel)
						.addComponent(middleInitialText)
						.addComponent(lastNameLabel)
						.addComponent(lastNameText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(ssnLabel)
						.addComponent(ssnText))
				.addGroup(formLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(dobLabel)
						.addComponent(dobText))
				.addComponent(submitBtn));
		
		
		submitBtn.addActionListener(new ActionListener()
				{

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						String name = firstNameText.getText() + " " + lastNameText.getText();
						patient.setFullName(name);
						patient.setPersonalData(ssnText.getText(), MyUtils.convertStringToDate(dobText.getText(), format));
						Menu.showDialog("Patient " + name + " has been successfully modified!");
						changePanel.accept(new Homepage(sd, changePanel), Homepage.title);
					}
				});
		remove.addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					if(remove.isSelected() == true)
					{
						String name = patient.getFirstName() + " " + patient.getLastName() ;
						int result = Menu.showConfirmationDialog("Are you sure you wish to remove " + name + "?");
						if(result == JOptionPane.YES_OPTION)
						{	
							remove.setSelected(true);
							patient.markAsRemoved();
							Menu.showDialog(name + " has been marked as removed");
						}
						else
							remove.setSelected(false);
					}
					else
					{
						String name = patient.getFirstName() + " " + patient.getLastName() ;
						int result = Menu.showConfirmationDialog("Are you sure you wish to unremove " + name  + "?");
						if(result == JOptionPane.YES_OPTION)
						{	
							remove.setSelected(false);
							patient.unRemove();
						}
						else
							remove.setSelected(true);
					}
				}
			});
	}

}
