/**
 * 
 */
package edu.miami.cis324.hw4.svargas;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import edu.miami.cis324.hw4.svargas.Utilities.PrintUtils;
import edu.miami.cis324.hw4.svargas.Utilities.SchedulerXMLReaderUtils;

/**
 * @author Sarah Vargas
 *
 */
public class SchedulerXMLReadTest {
	private final static String INPUT_FILE = "resources\\schedulerData.xml";
	
	/**
	 * @param args
	 * @throws XMLStreamException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, XMLStreamException {
		SchedulerData data = SchedulerXMLReaderUtils.readSchedulingXML(INPUT_FILE);
		PrintUtils.printVisits(data.getVisitList(), data.getPatientList(), data.getDoctorList());
	}

}
