/**
 * 
 */
package edu.miami.cis324.hw4.svargas;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import edu.miami.cis324.hw4.svargas.Utilities.SchedulerXMLReaderUtils;
import edu.miami.cis324.hw4.svargas.Utilities.SchedulerXMLWriterUtils;

/**
 * @author Sarah Vargas
 * created 4/8/2017
 *
 */
public class SchedulerXMLWriteTest {
	private final static String INPUT_FILE = "resources\\schedulerData.xml";
	private final static String OUTPUT_FILE = "resources\\schedulerOut.xml";
	
	/**
	 * @param args
	 * @throws XMLStreamException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws XMLStreamException, IOException {
		SchedulerData data = SchedulerXMLReaderUtils.readSchedulingXML(INPUT_FILE);
		SchedulerXMLWriterUtils.writeSchedulerXML(OUTPUT_FILE, data);
	}

}
