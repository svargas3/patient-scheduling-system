/**
 * 
 */
package edu.miami.cis324.hw4.svargas;

import java.util.ArrayList;
import java.util.Collection;

import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.Visit;

/**
 * @author Sarah Vargas
 * created 4/6/2017
 */
public class SchedulerData {
	private Collection<Patient> p_list;
	private Collection<Doctor> d_list;
	private Collection<Visit<Integer, Integer>> v_list;
	
	public SchedulerData()
	{
		p_list = new ArrayList<Patient>();// new TreeSet<Patient>();
		d_list = new ArrayList<Doctor>();//TreeSet<Doctor>();
		v_list = new ArrayList<Visit<Integer, Integer>>();
	}
	
	public Collection<Patient> getPatientList()
	{
		return p_list;
	}
	
	public Collection<Doctor> getDoctorList()
	{
		return d_list;
	}
	public Collection<Visit<Integer, Integer>> getVisitList()
	{
		return v_list;
	}
	
	public void addPatient(Patient p)
	{
		p_list.add(p);
	}
	
	public void addDoctor(Doctor d)
	{
		d_list.add(d);
	}
	
	public void addVisit(Visit<Integer, Integer> v)
	{
		v_list.add(v);
	}
	
	public void removePatient(Patient p)
	{
		p.markAsRemoved();
		//p_list.remove(p);
	}
	
	public void unremovePatient(Patient p)
	{
		p.unRemove();
		//p_list.remove(p);
	}
	
	
	public void removeDoctor(Doctor d)
	{
		d.markAsRemoved();
		//d_list.remove(d);
	}
	
	public void unremoveDoctor(Doctor d)
	{
		d.unRemove();
	}
	
	public void removeVisit(Visit<Integer, Integer> v)
	{
		v_list.remove(v);
	}
	
	public int getMaxPatIndex()
	{
		int max = 0;
		for(Patient p: p_list)
		{
			if(p.getPatientID() > max)
				max = p.getPatientID();
		}
		return max;
	}
	
	public int getMaxDocIndex()
	{
		int max = 0;
		for(Doctor d: d_list)
		{
			if(d.getDoctorID() > max)
				max = d.getDoctorID();
		}
		return max;
	}
	
	public Patient findPatient(int id)
	{
		for(Patient p : p_list)
		{
			if(p.getPatientID() == id)
				return p;
		}
		return null;
	}
	
	public Doctor findDoctor(int id)
	{
		for(Doctor d : d_list)
		{
			if(d.getDoctorID() == id)
				return d;
		}
		return null;
	}
}
