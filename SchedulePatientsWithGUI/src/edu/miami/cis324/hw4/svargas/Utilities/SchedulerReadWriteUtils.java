/**
 * 
 */
package edu.miami.cis324.hw4.svargas.Utilities;

/**
 * @author Sarah Vargas
 *
 */
public class SchedulerReadWriteUtils {
	protected final static String ROOT = "root";
	protected final static String PATIENT = "patient";
	protected final static String DOCTOR = "doctor";
	protected final static String VISIT = "visit";
	protected final static String PERSONAL_DATA = "data";
	protected final static String MEDICAL_SPECIALTY = "specialty";
	protected final static String DOCTORID = "doctorId";
	protected final static String PATIENTID = "patientId";
	protected final static String SSN = "SSN";
	protected final static String NAME = "name";
	protected final static String FIRST_NAME = "firstName";
	protected final static String LAST_NAME = "lastName";
	protected final static String DOB = "dob";
	protected final static String VISIT_DATE = "visitDate";
}
