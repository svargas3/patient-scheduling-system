/**
 * 
 */
package edu.miami.cis324.hw4.svargas.Utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.Visit;
import edu.miami.cis324.hw4.svargas.data.VisitComp;

/**
 * @author Sarah Vargas
 *
 */
public class PrintUtils {
	/**
	 * Prints all upcoming visits ordered by visit date
	 * @param visitList
	 * @param patientList
	 * @param doctorList
	 */
	public static void printVisits(Collection<Visit<Integer,Integer>> visitList, Collection<Patient> patientList, Collection<Doctor> doctorList)
	{
		SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
		Collections.sort((List<Visit<Integer,Integer>>)visitList, new VisitComp<Integer, Integer>());
		
		for(Visit<Integer, Integer> vii: visitList)
		{
			if(vii.getVisitDate().after(Calendar.getInstance().getTime()))
			{
				Patient pat = MyUtils.findPatientByID(vii.getVisitor().intValue(), patientList);
				Doctor doc = MyUtils.findDoctorByID(vii.getHost().intValue(), doctorList);
				System.out.printf("%-20s%-20s%n", "Visit date: ", df.format(vii.getVisitDate()));
				System.out.printf("%-20s%-40s%n", "Doctor: ", doc.getFirstName() + " " + doc.getLastName());
				System.out.printf("%-20s%-20s%n", "Specialty: ", doc.getSpecialty());
				System.out.printf("%-20s%-20s%n", "Days until visit", MyUtils.dateDiff(vii.getVisitDate(), Calendar.getInstance().getTime()));
				System.out.println("Patient: ");
				System.out.printf("\t%-20s%-20s%n", "First name: ", pat.getFirstName());
				System.out.printf("\t%-20s%-20s%n", "Last name: ", pat.getLastName());
				System.out.printf("\t%-20s%-20s%n", "SSN: ", pat.getSSN());
				System.out.printf("\t%-20s%-20d%n", "Age: ", pat.getAge());
				System.out.println();
			}	
		}
	}
	
	
}
