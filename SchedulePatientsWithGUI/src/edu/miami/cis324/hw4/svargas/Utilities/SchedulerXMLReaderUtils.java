package edu.miami.cis324.hw4.svargas.Utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.DoctorImpl;
import edu.miami.cis324.hw4.svargas.data.Name;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.PatientImpl;
import edu.miami.cis324.hw4.svargas.data.PersonalData;
import edu.miami.cis324.hw4.svargas.data.Visit;
import edu.miami.cis324.hw4.svargas.data.VisitImpl;
/**
 * @author Sarah Vargas
 * created 4/6/2017
 */

public final class SchedulerXMLReaderUtils extends SchedulerReadWriteUtils{

	private final static String DOB_FORMAT = "yyyy-MM-dd";
	
	public static Name readName(XMLEventReader eventReader) throws XMLStreamException
	{
		Name fullName = null;
		String firstName = null, lastName = null;
		boolean finished = false;
		XMLEvent event = eventReader.nextEvent();
		while(!finished)
		{
			if(event.isStartElement())
			{
				StartElement startElement = event.asStartElement();
				String localPart = startElement.getName().getLocalPart();
				switch(localPart) {
					case FIRST_NAME:
						firstName = eventReader.nextEvent().asCharacters().getData();
						event = eventReader.nextEvent();
						break;
					case LAST_NAME:
						lastName = eventReader.nextEvent().asCharacters().getData();
						event = eventReader.nextEvent();
						break;
					default: 
					{	System.err.println("Found unknown element, ignoring; found: " + localPart);
						event = eventReader.nextEvent();
					}
				}
			}
			else if(event.isEndElement())
			{
				EndElement endElement = event.asEndElement();
				if(endElement.getName().getLocalPart().equals(NAME))
				{
					fullName = new Name(firstName, lastName);
					finished = true;
				}
				else
					event = eventReader.nextEvent();
			}
			else
				event = eventReader.nextEvent();
		}
		return fullName;
	}
	
	public static PersonalData readPersonalData(XMLEventReader eventReader) throws XMLStreamException
	{
		PersonalData data = null;
		Date dob = null;
		DateFormat sdf = new SimpleDateFormat(DOB_FORMAT);
		String date;
		String ssn = null;
		boolean finished = false;
		XMLEvent event = eventReader.nextEvent();
		while(!finished)
		{
			if(event.isStartElement())
			{
				StartElement startElement = event.asStartElement();
				String localPart = startElement.getName().getLocalPart();
				switch(localPart) {
					case DOB:
						date = eventReader.nextEvent().asCharacters().getData();
						try{
							dob = sdf.parse(date);
						} catch(ParseException e){
							e.printStackTrace();
						}
						event = eventReader.nextEvent();
						break;
					case SSN:
						ssn = eventReader.nextEvent().asCharacters().getData();
						event = eventReader.nextEvent();
						break;
					default: 
						System.err.println("Found unknown element, ignoring; found: " + localPart);
						event = eventReader.nextEvent();
				}
			}
			else if(event.isEndElement())
			{
				EndElement endElement = event.asEndElement();
				if(endElement.getName().getLocalPart().equals(PERSONAL_DATA))
				{
					data = new PersonalData(dob, ssn);
					finished = true;
				}
				else
					event = eventReader.nextEvent();
			}
			else
				event = eventReader.nextEvent();
		}
		return data;
	}
	
	public static Doctor readDoctor(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException
	{
		Doctor doc = null;
		Name fullName = null;
		PersonalData data = null;
		String medicalSpecialty = null;
		int id = -1;
		StartElement startElement;
		if(event.isStartElement())
		{
			startElement = event.asStartElement();
			@SuppressWarnings("unchecked")
			Iterator<Attribute> attributes = startElement.getAttributes();
			while(attributes.hasNext())
			{
				Attribute attribute = attributes.next();
				if(attribute.getName().getLocalPart().equals(DOCTORID))
					id = Integer.valueOf(attribute.getValue());
				else 
					System.err.println("Found unknown attribute, ignoring; found: " + attribute.getName());
			}
		}	
		
		event = eventReader.nextEvent();
		boolean finished = false;
		while(!finished)
		{
			if(event.isStartElement())
			{
				startElement = event.asStartElement();
				String localPart = startElement.getName().getLocalPart();
				switch(localPart) {
					case NAME:
						fullName = readName(eventReader);
						event = eventReader.nextEvent();
						break;
					case PERSONAL_DATA:
						data = readPersonalData(eventReader);
						event = eventReader.nextEvent();
						break;
					case MEDICAL_SPECIALTY:
						medicalSpecialty = eventReader.nextEvent().asCharacters().getData();
						event = eventReader.nextEvent();
						break;
					default: 
					{	System.err.println("Found unknown element, ignoring; found: " + localPart);
						event = eventReader.nextEvent(); }
				}
			}
			else if(event.isEndElement())
			{
				EndElement endElement = event.asEndElement();
				if(endElement.getName().getLocalPart().equals(DOCTOR))
				{
					doc = new DoctorImpl(fullName, data, medicalSpecialty, id);
					finished = true;
				}
				else
					event = eventReader.nextEvent();
			}
			else
				event = eventReader.nextEvent();
		}
		return doc;
	}
	
	public static Patient readPatient(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException
	{
		Patient pat = null;
		Name fullName = null;
		PersonalData data = null;
		int id = -1;
		StartElement startElement;
		if(event.isStartElement())
		{
			startElement = event.asStartElement();
			@SuppressWarnings("unchecked")
			Iterator<Attribute> attributes = startElement.getAttributes();
			while(attributes.hasNext())
			{
				Attribute attribute = attributes.next();
				if(attribute.getName().getLocalPart().equals(PATIENTID))
					id = Integer.valueOf(attribute.getValue());
				else 
					System.err.println("Found unknown attribute, ignoring; found: " + attribute.getName());
			}
		}	
		
		event = eventReader.nextEvent();
		boolean finished = false;
		while(!finished)
		{
			if(event.isStartElement())
			{
				startElement = event.asStartElement();
				String localPart = startElement.getName().getLocalPart();
				switch(localPart) {
					case NAME:
						fullName = readName(eventReader);
						event = eventReader.nextEvent();
						break;
					case PERSONAL_DATA:
						data = readPersonalData(eventReader);
						event = eventReader.nextEvent();
						break;
					default: 
						System.err.println("Found unknown element, ignoring; found: " + localPart);
						event = eventReader.nextEvent();
				}
			}
			else if(event.isEndElement())
			{
				EndElement endElement = event.asEndElement();
				if(endElement.getName().getLocalPart().equals(PATIENT))
				{
					pat = new PatientImpl(fullName, data, id);
					finished = true;
				}
				else
					event = eventReader.nextEvent();
			}
			else
				event = eventReader.nextEvent();
		}
		return pat;
	}
	
	public static Visit<Integer, Integer> readVisit(XMLEvent event, XMLEventReader eventReader)
	{
		Visit<Integer, Integer> v = null;
		int patientId = -1, doctorId = -1;
		Date visitDate = null;
		String date;
		DateFormat sdf = new SimpleDateFormat(DOB_FORMAT);
		if(event.isStartElement())
		{
			StartElement startElement = event.asStartElement();
			@SuppressWarnings("unchecked")
			Iterator<Attribute> attributes = startElement.getAttributes();
			while(attributes.hasNext())
			{
				Attribute attribute = attributes.next();
				if(attribute.getName().getLocalPart().equals(PATIENTID))
					patientId = Integer.valueOf(attribute.getValue());
				else if(attribute.getName().getLocalPart().equals(DOCTORID))
					doctorId = Integer.valueOf(attribute.getValue());
				else if(attribute.getName().getLocalPart().equals(VISIT_DATE))
				{
					date = attribute.getValue();
					try {
						visitDate = sdf.parse(date);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				else 
					System.err.println("Found unknown attribute, ignoring; found: " + attribute.getName());
			}
			v = new VisitImpl<Integer, Integer>(patientId, doctorId, visitDate);
		}	
		return v;
	}
	
	public static SchedulerData readSchedulingXML(String filename) throws XMLStreamException, FileNotFoundException
	{
		SchedulerData data = new SchedulerData();
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		Reader in = new BufferedReader(new FileReader(filename));
		XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

		while(eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if(event.isStartElement())
			{
				String localPart = event.asStartElement().getName().getLocalPart();
				switch(localPart)
				{
					case ROOT:
						continue;
					case DOCTOR:
						Doctor d = readDoctor(event, eventReader);
						data.addDoctor(d);
						break;
					case PATIENT:
						Patient p = readPatient(event, eventReader);
						data.addPatient(p);
						break;
					case VISIT:
						Visit<Integer,Integer> v = readVisit(event, eventReader);
						data.addVisit(v);
						break;
					default: 
						System.err.println("Unrecognized element, ignoring: " +  localPart);
						event = eventReader.nextEvent();
				}
			}
		}
		eventReader.close();
		return data;
	}
}
