/**
 * 
 */
package edu.miami.cis324.hw4.svargas.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.Patient;

/**
 * @author Sarah
 * created 4/8/2017
 */
public class MyUtils {

	/**
	 * Finds a patient in a list based on the given patient ID
	 * @param id
	 * @param p_list
	 * @return
	 */
	public static Patient findPatientByID(int id, Collection<Patient> p_list)
	{
		for(Patient p: p_list)
		{
			if(p.getPatientID() == id)
				return p;
		}
		return null;
	}
	
	/**
	 * Finds a doctor in a list based on the given doctor ID
	 * @param id
	 * @param d_list
	 * @return
	 */
	public static Doctor findDoctorByID(int id, Collection<Doctor> d_list)
	{
		for(Doctor d: d_list)
		{
			if(d.getDoctorID() == id)
				return d;
		}
		return null;
	}
	
	/**
	 * Calculates number of days between two dates
	 * @param date1
	 * @param date2
	 * @return int: days between 2 dates
	 */
	public static int dateDiff(Date date1, Date date2)
	{
		long diff = date1.getTime() - date2.getTime();
		return (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1;
	}
	/**
	 * Converts string form of date to a Calendar object given the format
	 * @param String date
	 * @param String format
	 * @return Calendar object
	 * @throws ParseException
	 */
	public static Date convertStringToDate(String date, String format)
	{
		DateFormat sdf = new SimpleDateFormat(format);
		Date dob = Calendar.getInstance().getTime();
		try {
			dob = (Date)sdf.parse(date);
		} catch (ParseException e) {
			System.out.println("String not in valid form. Cannot be converted to Calendar");
		}
		return dob;
	}
	
	/**
	 * Converts Calendar form of date to a String given the format
	 * @param Calendar date 
	 * @param String df : format of the date
	 * @return String
	 */
	public static String convertDateToString(Date date, String df)
	{
		DateFormat sdf = new SimpleDateFormat(df);
		String dob = sdf.format(date);
		return dob;
	}
	
}
