/**
 * 
 */
package edu.miami.cis324.hw4.svargas.Utilities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;

import edu.miami.cis324.hw4.svargas.SchedulerData;
import edu.miami.cis324.hw4.svargas.data.Doctor;
import edu.miami.cis324.hw4.svargas.data.Patient;
import edu.miami.cis324.hw4.svargas.data.Person;
import edu.miami.cis324.hw4.svargas.data.PersonalData;
import edu.miami.cis324.hw4.svargas.data.Visit;

/**
 * @author Sarah Vargas
 *
 */
public final class SchedulerXMLWriterUtils extends SchedulerReadWriteUtils {
	private final static String NAMESPACE = "http://www.miami.edu/cis324/xml/scheduling";
	private final static String SCHEMA_INSTANCE_PREFIX = "xsi";
	private final static String SCHEMA_INSTANCE_NS = "http://www.w3.org/2001/XMLSchema-instance";
	private final static String SCHEMA_PREFIX = "xsd";
	private final static String SCHEMA = "http://www.w3.org/2001/XMLSchema";
	private final static String SCHEMA_LOCATION_ATTRNAME = "schemaLocation";
	private final static String SCHEMA_FILE_NAME = "scheduling.xsd";
	private final static String DATE_FORMAT = "yyyy-MM-dd";
	private final static DateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
	
	public static Characters getIndentation(XMLEventFactory eventFactory, int level) {
		// returns an object with as many tabs as needed to indent to the value specified by the input parameter
		char[] tabs = new char[level];
		Arrays.fill(tabs, '\t'); // fill the number of tabs
		return eventFactory.createIgnorableSpace(String.valueOf(tabs)); // and create an ignorable space
	}
	
	public static void writeNode(XMLEventWriter eventWriter, XMLEventFactory eventFactory, String name, String value, int level) throws XMLStreamException
	{
		eventWriter.add(getIndentation(eventFactory, level));
		eventWriter.add(eventFactory.createStartElement("", "", name));
		eventWriter.add(eventFactory.createCharacters(value));
		eventWriter.add(eventFactory.createEndElement("", "", name));
		eventWriter.add(eventFactory.createIgnorableSpace("\n"));
	}
	
	public static void writePersonalDataXML(XMLEventWriter eventWriter, XMLEventFactory eventFactory, PersonalData data, int level) throws XMLStreamException
	{
		eventWriter.add(getIndentation(eventFactory, level));
		eventWriter.add(eventFactory.createStartElement("", "", PERSONAL_DATA));
		eventWriter.add(eventFactory.createIgnorableSpace("\n"));
		writeNode(eventWriter, eventFactory, DOB, sdf.format(data.getDOB()), level + 1);
		writeNode(eventWriter, eventFactory, SSN, data.getSSN(), level + 1);
		eventWriter.add(getIndentation(eventFactory, level));
		eventWriter.add(eventFactory.createEndElement("", "", PERSONAL_DATA));
		eventWriter.add(eventFactory.createIgnorableSpace("\n"));
	}
	
	public static <T extends Person> void writeNameXML(XMLEventWriter eventWriter, XMLEventFactory eventFactory, T obj, int level) throws XMLStreamException
	{
		eventWriter.add(getIndentation(eventFactory, level));
		eventWriter.add(eventFactory.createStartElement("", "", NAME));
		eventWriter.add(eventFactory.createIgnorableSpace("\n"));
		writeNode(eventWriter, eventFactory, FIRST_NAME, obj.getFirstName(), level + 1);
		writeNode(eventWriter, eventFactory, LAST_NAME, obj.getLastName(), level + 1);
		eventWriter.add(getIndentation(eventFactory, level));
		eventWriter.add(eventFactory.createEndElement("", "", NAME));
		eventWriter.add(eventFactory.createIgnorableSpace("\n"));
	}
	
	public static void writeDoctorXML(XMLEventWriter eventWriter, XMLEventFactory eventFactory, Collection<Doctor> doctorList, int level) throws XMLStreamException
	{
		for(Doctor d : doctorList)
		{
			eventWriter.add(getIndentation(eventFactory, level));
			StartElement doctorStart = eventFactory.createStartElement("", "", DOCTOR);
			eventWriter.add(doctorStart);
			Attribute doctorId = eventFactory.createAttribute(DOCTORID, Integer.toString(d.getDoctorID()));
			eventWriter.add(doctorId);
			eventWriter.add(eventFactory.createIgnorableSpace("\n"));
			writeNameXML(eventWriter, eventFactory, d, level + 1);
			writePersonalDataXML(eventWriter, eventFactory, d.getPersonalData(), level + 1);
			writeNode(eventWriter, eventFactory, MEDICAL_SPECIALTY, d.getSpecialty().toString(), level + 1);
			eventWriter.add(getIndentation(eventFactory, level));
			eventWriter.add(eventFactory.createEndElement("", "", DOCTOR));
			eventWriter.add(eventFactory.createIgnorableSpace("\n"));
		}
	}
	
	public static void writePatientXML(XMLEventWriter eventWriter, XMLEventFactory eventFactory, Collection<Patient> patientList, int level) throws XMLStreamException
	{
		for(Patient p : patientList)
		{
			eventWriter.add(getIndentation(eventFactory, level));
			StartElement patientStart = eventFactory.createStartElement("", "", PATIENT);
			eventWriter.add(patientStart);
			Attribute patientId = eventFactory.createAttribute(PATIENTID, Integer.toString(p.getPatientID()));
			eventWriter.add(patientId);
			eventWriter.add(eventFactory.createIgnorableSpace("\n"));
			writeNameXML(eventWriter, eventFactory, p, level + 1);
			writePersonalDataXML(eventWriter, eventFactory, p.getPersonalData(), level + 1);
			eventWriter.add(getIndentation(eventFactory, level));
			eventWriter.add(eventFactory.createEndElement("", "", PATIENT));
			eventWriter.add(eventFactory.createIgnorableSpace("\n"));
		}
	}
	
	public static void writeVisitXML(XMLEventWriter eventWriter, XMLEventFactory eventFactory, Collection<Visit<Integer, Integer>> visitList, int level) throws XMLStreamException
	{
		for(Visit<Integer, Integer> v : visitList)
		{
			eventWriter.add(getIndentation(eventFactory, level));
			StartElement visitStart = eventFactory.createStartElement("", "", VISIT);
			eventWriter.add(visitStart);
			Attribute patientId = eventFactory.createAttribute(PATIENTID, Integer.toString(v.getVisitor()));
			Attribute doctorId = eventFactory.createAttribute(DOCTORID, Integer.toString(v.getHost()));
			Attribute visitDate = eventFactory.createAttribute(VISIT_DATE, sdf.format(v.getVisitDate()));
			
			eventWriter.add(patientId);
			eventWriter.add(doctorId);
			eventWriter.add(visitDate);
			eventWriter.add(eventFactory.createEndElement("", "", VISIT));
			eventWriter.add(eventFactory.createIgnorableSpace("\n"));
		}
	}
	
	public static void writeSchedulerXML(String outFile, SchedulerData data) throws IOException, XMLStreamException
	{
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		Writer out = new BufferedWriter(new FileWriter(outFile));
		XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(out);
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		int level = 1;
		
		StartDocument startDocument = eventFactory.createStartDocument("UTF-8", "1.0");
		eventWriter.add(startDocument);
		eventWriter.add(eventFactory.createIgnorableSpace("\n"));
		eventWriter.add(eventFactory.createStartElement("", "", ROOT));
		eventWriter.setDefaultNamespace(NAMESPACE);
		eventWriter.add(eventFactory.createNamespace(SCHEMA_INSTANCE_PREFIX, SCHEMA_INSTANCE_NS));
		eventWriter.add(eventFactory.createNamespace(SCHEMA_PREFIX, SCHEMA));
		eventWriter.add(eventFactory.createNamespace(NAMESPACE));
		eventWriter.add(eventFactory.createAttribute(SCHEMA_INSTANCE_PREFIX, SCHEMA_INSTANCE_NS 
								, SCHEMA_LOCATION_ATTRNAME, NAMESPACE + " " + SCHEMA_FILE_NAME));
		eventWriter.add(eventFactory.createIgnorableSpace("\n"));
		writeDoctorXML(eventWriter, eventFactory, data.getDoctorList(), level);
		writePatientXML(eventWriter, eventFactory, data.getPatientList(), level);
		writeVisitXML(eventWriter, eventFactory, data.getVisitList(), level);
		eventWriter.add(eventFactory.createEndElement("", "", ROOT));
		eventWriter.add(eventFactory.createEndDocument());
		eventWriter.close();
	}
}
