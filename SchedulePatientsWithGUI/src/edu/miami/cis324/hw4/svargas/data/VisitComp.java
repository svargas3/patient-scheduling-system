package edu.miami.cis324.hw4.svargas.data;

import java.util.Comparator;

/**
 * @author Sarah Vargas
 * created: 3/02/2017
 * last modified: 3/02/2017
 *
 * External Comparator implementation to sort visits by Date
 */

public class VisitComp<V,T> implements Comparator<Visit<V, T>> {
	@Override
	public int compare(Visit<V, T> v1, Visit<V, T> v2) {
		return v1.getVisitDate().compareTo(v2.getVisitDate());
	}
}
