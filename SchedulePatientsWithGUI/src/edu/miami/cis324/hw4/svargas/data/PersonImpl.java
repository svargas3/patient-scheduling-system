package edu.miami.cis324.hw4.svargas.data;
import java.util.Date;

/**
 * Contains getters/setters that apply to all subclasses
 * @author Sarah Vargas
 * created 4/6/2017
 *
 * last modified: 4/8/2017
 */
public abstract class PersonImpl implements Person{
		protected PersonalData data;
		protected Name name;
		protected boolean isRemoved;
		
		PersonImpl(Name fullName, PersonalData pd)
		{
			name = fullName;
			data = pd;
			isRemoved = false;
		}
		
		PersonImpl(String fullName, String SSN, Date birthDate)
		{
			String [] temp = fullName.split(" ");
			name = new Name(temp[0], temp[1]);
			data = new PersonalData(birthDate, SSN);
			isRemoved = false;
		}
		
		@Override
		public String getLastName() {
			return name.getLastName();
		}

		@Override
		public String getFirstName() {
			return name.getFirstName();
		}

		@Override
		public String getSSN() {
			return data.getSSN();
		}

		@Override
		public Date getDOB() {
			return data.getDOB();
		}
		
		@Override
		public PersonalData getPersonalData() {
			return data;
		}
		@Override
		public Name getFullName() {
			return name;
		}
		
		@Override
		public void setFullName(String fullName)
		{
			String [] temp = fullName.split(" ");
			name.setFirstName(temp[0]);
			name.setLastName(temp[1]);
		}
		
		@Override
		public void setPersonalData(String ssn, Date dob)
		{
			data.setDOB(dob);
			data.setSSN(ssn);
		}
		
		public boolean isRemoved()
		{
			return isRemoved;
		}

		public void markAsRemoved()
		{
			isRemoved = true;
		}
		
		public boolean unRemove()
		{
			isRemoved = false;
			return true; 
		}
}
