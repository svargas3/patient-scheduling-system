/**
 * 
 */
package edu.miami.cis324.hw4.svargas.data;

/**
 * @author Sarah Vargas
 * created: 2/20/2017
 *
 * Patient Interface
 */
public interface Patient extends Person {
	public int getPatientID();
	public int getAge();
}
