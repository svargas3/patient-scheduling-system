package edu.miami.cis324.hw4.svargas.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PersonalData {
	private Date dob;
	private String ssn;
	
	public PersonalData(Date birthDate, String ssn)
	{
		dob = birthDate;
		this.ssn = ssn;
	}

	public Date getDOB() {
		return dob;
	}

	public String getSSN() {
		return ssn;
	}
	
	public void setDOB(Date dob) {
		this.dob = dob;
	}

	public void setSSN(String ssn) {
		this.ssn = ssn;
	}

	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
		return sdf.format(dob) + " " + ssn;
	}
}
