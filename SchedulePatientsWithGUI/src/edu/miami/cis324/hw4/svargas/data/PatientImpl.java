/**
 * 
 */
package edu.miami.cis324.hw4.svargas.data;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Sarah Vargas
 * created: 2/20/2017
 * last modified: 3/03/2017
 *
 */
public class PatientImpl extends PersonImpl implements Patient, Comparable<PatientImpl> {
	
	private int patientId;
	private static int IDcounter = 1;
	
	public PatientImpl(Name fullName, PersonalData data)
	{
		super(fullName, data);
		patientId = IDcounter;
		IDcounter += 1;
	}
	
	public PatientImpl(Name fullName, PersonalData data, int id)
	{
		super(fullName, data);
		patientId = id;
	}
	
	public PatientImpl(String fullName, String ssn, Date birthDate)
	{
		super(fullName, ssn, birthDate);
		patientId = IDcounter;
		IDcounter += 1;
	}
	
	public int getPatientID() {
		return patientId;
	}
	
	public static void setIDcounter(int count)
	{
		IDcounter = count;
	}
	
	
	/**
	 * Calculates age
	 * @return int Age
	 */
	@Override
	public int getAge() 
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(getDOB());
		int birthYear = cal.get(Calendar.YEAR);
		Calendar currentDate = Calendar.getInstance();
		int currentYear = currentDate.get(Calendar.YEAR);
		int diff = currentYear - birthYear;
		if (cal.get(Calendar.DAY_OF_YEAR) < currentDate.get(Calendar.DAY_OF_YEAR))
			return diff;
		return diff - 1;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		Patient p = null;
		if(obj instanceof Patient)
			p = (Patient)obj;
		else
			return false;
		if(this == p)
			return true;
		return (this.patientId == p.getPatientID()) && 
				(this.getLastName().equals(p.getLastName())) &&
				(this.getFirstName().equals(p.getFirstName())) &&
				(this.getSSN().equals(p.getSSN())) && (this.getDOB().equals(p.getDOB()));
	}
	
	@Override
	public int hashCode()
	{
		int result = 0;
		result += patientId;
		result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
		result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
		result = 31 * result + (getSSN() != null ? getSSN().hashCode() : 0);
		result = 31 * result + (getDOB() != null ? getDOB().hashCode() : 0);
		return result;
	}
	
	/**
	 * Compares this PatientImpl to another PatientImpl based on the attributes in this order (first to last):
	 * last name, first name, date of birth, SSN
	 */
	public int compareTo(PatientImpl d2)
	{
		if(getLastName().compareTo(d2.getLastName()) == 0)
		{
			if(this.getFirstName().compareTo(d2.getFirstName()) == 0)
			{
				if(this.getDOB().compareTo(d2.getDOB()) == 0)
				{
					if(this.getSSN().compareTo(d2.getSSN()) == 0)
						return 0;
					else if(this.getSSN().compareTo(d2.getSSN()) < 0)
						return -1;
					else
						return 1;
				}
				else if(this.getDOB().compareTo(d2.getDOB()) < 0)
					return -1;
				else
					return 1;
			}
			else if(this.name.getFirstName().compareTo(d2.getFirstName()) < 0)
				return -1;
			else
				return 1;
		}
		else if(name.getLastName().compareTo(d2.getLastName()) < 0)
			return -1;
		else
			return 1;
	}
	
	public String toString()
	{
		return name.getFirstName() + " " + name.getLastName();
	}
}
