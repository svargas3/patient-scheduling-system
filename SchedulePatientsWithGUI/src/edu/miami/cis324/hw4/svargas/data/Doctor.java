/**
 * 
 */
package edu.miami.cis324.hw4.svargas.data;

/**
 * @author Sarah Vargas
 * created: 2/20/2017
 * last modified: 2/20/2017
 *
 */
public interface Doctor extends Person{
	int getDoctorID();
	//String getLastName();
	//String getFirstName();
	MedicalSpecialty getSpecialty();
	void setSpecialty(MedicalSpecialty ms);
	//PersonalData getPersonalData();
	//String getSSN();
	//Date getDOB();
}
