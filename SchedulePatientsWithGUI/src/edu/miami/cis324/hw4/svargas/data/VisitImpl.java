package edu.miami.cis324.hw4.svargas.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Sarah Vargas
 * created: 2/20/2017
 * last modified: 3/03/2017
 *
 * Generic Visit Implementation Class 
 */

public class VisitImpl<V, T> implements Visit<V, T>  {
	
	private V visitor;
	private T host;
	private Date visitDate;
	
	public VisitImpl(V v, T h, Date vd)
	{
		visitor = v;
		host = h;
		visitDate = vd;
	}
	@Override
	public V getVisitor() {
		return visitor;
	}

	@Override
	public T getHost() {
		return host;
	}

	@Override
	public Date getVisitDate() {
		return visitDate;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		@SuppressWarnings("unchecked")
		Visit<V,T> v = (Visit<V,T>)obj;
		if(v == null)
			return false;
		if(this == v)
			return true;
		return (this.visitor.equals(v.getVisitor())) &&
				(this.host.equals(v.getHost()) &&
				(this.visitDate.equals(v.getVisitDate())));
	}
	
	@Override
	public int hashCode()
	{
		int result = 0;
		if(visitor != null)
			result = visitor.hashCode();
		result = 31 * result + (host != null ? host.hashCode() : 0);
		result = 31 * result + (visitDate != null ? visitDate.hashCode() : 0);
		return result;
	}
	
	public String toString()
	{
		DateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm a");
		return visitor.toString() + " " + host.toString() + " " + sdf.format(visitDate);
	}
}
