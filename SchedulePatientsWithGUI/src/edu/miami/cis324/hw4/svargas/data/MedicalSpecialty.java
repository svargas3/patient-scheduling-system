/**
 * 
 */
package edu.miami.cis324.hw4.svargas.data;

/**
 * @author Sarah Vargas
 * created: 2/20/2017
 * last modified: 2/20/2017
 * 
 * Medical specialty enumeration
 */
public enum MedicalSpecialty {
	GENERAL_MEDICINE, PEDIATRICS, ONCOLOGY;
	
	/**
	 * Converts a string to a MedicalSpecialty
	 * @param spec
	 * @return MedicalSpecialty
	 */
	public static MedicalSpecialty getFromString(String spec)
	{
		return valueOf(spec.toUpperCase());
	}
}
