/**
 * 
 */
package edu.miami.cis324.hw4.svargas.data;

import java.util.Date;

/**
 * @author Sarah Vargas
 * created: 4/6/2017
 * last modified: 4/8/2017
 */
public interface Person {
	String getLastName();
	String getFirstName();
	String getSSN();
	Date getDOB();
	void setFullName(String fullName);
	void setPersonalData(String ssn, Date dob);
	PersonalData getPersonalData();
	Name getFullName();
	void markAsRemoved();
	boolean unRemove();
	boolean isRemoved();
}
