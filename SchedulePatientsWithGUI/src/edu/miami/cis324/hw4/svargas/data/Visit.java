package edu.miami.cis324.hw4.svargas.data;

import java.util.Date;

/**
 * @author Sarah Vargas
 * created: 2/20/2017
 *
 * Generic Visit Interface
 */

public interface Visit<V, T> {
	public V getVisitor();
	public T getHost();
	public Date getVisitDate();
}
