package edu.miami.cis324.hw4.svargas.data;

import java.util.Date;

/**
 * @author Sarah Vargas
 * created: 2/20/2017
 * last modified: 3/03/2017
 *
 */

public class DoctorImpl extends PersonImpl implements Doctor {

	private int doctorId;
	private static int IDcounter = 1;
	private MedicalSpecialty specialty;
	
	/**
	 * Overloaded constructor that takes a string for the medical specialty
	 * @param fullName
	 * @param ssn
	 * @param birthDate
	 * @param spec
	 */
	public DoctorImpl(Name fullName, PersonalData pd, String spec)
	{
		super(fullName, pd);
		doctorId = IDcounter;
		IDcounter += 1;
		specialty = MedicalSpecialty.getFromString(spec);
		isRemoved = false;
	}
	
	/**
	 * Overloaded constructor that takes a string for the medical specialty
	 * @param fullName
	 * @param ssn
	 * @param birthDate
	 * @param spec
	 */
	public DoctorImpl(Name fullName, PersonalData pd, String spec, int id)
	{
		super(fullName, pd);
		doctorId = id;
		specialty = MedicalSpecialty.getFromString(spec);
		isRemoved = false;
	}
	
	/**
	 * Overloaded constructor that takes a string for the medical specialty
	 * @param fullName
	 * @param ssn
	 * @param birthDate
	 * @param spec
	 */
	public DoctorImpl(String fullName, String ssn, Date birthDate, String spec)
	{
		super(fullName, ssn, birthDate);
		doctorId = IDcounter;
		IDcounter += 1;
		specialty = MedicalSpecialty.getFromString(spec);
		isRemoved = false;
	}
	
	/**
	 * Overloaded constructor that takes the medical specialty as the MedicalSpecialty enum type
	 * @param fullName
	 * @param ssn
	 * @param birthDate
	 * @param spec
	 */
	public DoctorImpl(String fullName, String ssn, Date birthDate, MedicalSpecialty spec)
	{
		super(fullName, ssn, birthDate);
		doctorId = IDcounter;
		IDcounter += 1;
		specialty = spec;
		isRemoved = false;
	}
	
	/**
	 * Overloaded constructor that takes the medical specialty as the MedicalSpecialty enum type
	 * @param fullName
	 * @param ssn
	 * @param birthDate
	 * @param spec
	 */
	public DoctorImpl(String fullName, String ssn, Date birthDate, MedicalSpecialty spec, int id)
	{
		super(fullName, ssn, birthDate);
		doctorId = id;
		specialty = spec;
		isRemoved = false;
	}
	
	public static void setIDCounter(int count)
	{
		IDcounter = count;
	}
	
	@Override
	public int getDoctorID() {
		return doctorId;
	}

	@Override
	public MedicalSpecialty getSpecialty() {
		return specialty;
	}
	
	@Override
	public void setSpecialty(MedicalSpecialty ms) {
		specialty = ms;
	}

	public PersonalData getPersonalData()
	{
		return data;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(!(obj instanceof Doctor))
			return super.equals(obj);
		Doctor d = (Doctor)obj;
		if(this == d)
			return true;
		return (this.doctorId == d.getDoctorID()) && 
				(this.getLastName().equals(d.getLastName())) &&
				(this.getFirstName().equals(d.getFirstName())) &&
				(this.getSSN().equals(d.getSSN())) && (this.getDOB().equals(d.getDOB()) &&
				(this.specialty.equals(getSpecialty())));
	}
	
	@Override
	public int hashCode()
	{
		int result = 0;
		result += doctorId;
		result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
		result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
		result = 31 * result + (getSSN() != null ? getSSN().hashCode() : 0);
		result = 31 * result + (getDOB() != null ? getDOB().hashCode() : 0);
		result = 31 * result + (specialty != null ? specialty.hashCode() : 0);
		return result;
	}
	
	public String toString()
	{
		return getFirstName() + " " + getLastName();
	}
}
